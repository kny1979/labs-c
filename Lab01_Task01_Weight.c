#include <stdio.h>

int main()
{
	char gender;
	int height, weight;
	float queletet, heightm;

	puts("What is your gender? (M - male, F - female)");
	scanf("%c", &gender);
	puts("What is your height in centimeters?");
	scanf("%d", &height);
	puts("What is your weight in kilogramms?");
	scanf("%d", &weight);

	puts("\nCalculating of body mass index according to the formula Quetelet:");
	heightm = (float)height / 100; // Cm to m 
	queletet = weight / (heightm*heightm); // Calculate Queletet index
	if (queletet <= 16)
		printf("Your Queletet index is - %.2f. Pronounced underweight!\n\n", queletet);
	else if (queletet > 16 && queletet <= 18)
		printf("Your Queletet index is - %.2f. Lack (deficiency) body weight!\n\n", queletet);
	else if (queletet > 18 && queletet <= 25)
		printf("Your Queletet index is - %.2f. Your weight is normal!\n\n", queletet);
	else if (queletet > 25 && queletet <= 30)
		printf("Your Queletet index is - %.2f. Overweight (pre-obese)!\n\n", queletet);
	else if (queletet > 30 && queletet <= 35)
		printf("Your Queletet index is - %.2f. Obesity first degree!\n\n", queletet);
	else if (queletet > 35 && queletet <= 40)
		printf("Your Queletet index is - %.2f. Obesity second degree!\n\n", queletet);
	else if (queletet > 40)
		printf("Your Queletet index is - %.2f. Obesity third degree (morbid)!\n\n", queletet);

	puts("A simple analisys of the ratio of height and weight");
	switch(gender)
	{
	case'M':
		height -=100;
		printf("Your ideal weight is: %d kg\n", height);
		if ((height - 10) > weight)
			puts("You need to grow fat");
		else if ((height + 10) < weight)
			puts("You need to lose weight");
		else
			puts("You have normal weight");
		break;
	case'F':
		height -=110;
		printf("Your ideal weight is: %d kg\n", height);
		if ((height - 10) > weight)
			puts("You need to grow fat");
		else if ((height + 10) < weight)
			puts("You need to lose weight");
		else
			puts("You have normal weight");
		break;
	}//switch

	return 0;
}//main