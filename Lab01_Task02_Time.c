#include <stdio.h>

int main()
{
	int hh, mm, ss;
	puts("Enter the current time in the format HH:MM:SS");
	scanf("%d:%d:%d", &hh, &mm, &ss);
	if (hh > 23 || mm > 59 || ss > 59)
		puts("Invalid value of time!");
	else if (hh >= 6 && hh < 12)
		puts("Good morning!");
	else if (hh >= 12 && hh < 18)
		puts("Good day!");
	else if (hh >= 18 && hh <= 23)
		puts("Good evening!");
	else if (hh >= 0 && hh < 6)
		puts("Good night!");
	return 0;
}//main