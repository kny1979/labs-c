#include <stdio.h>
#define PI 3.14159265

int main()
{
	char gr;
	float value;

	puts("Enter a value in radians(R) or degrees(D) (Format %f%c)");
	scanf("%f%c", &value, &gr);

	switch(gr)
	{
	case'D':
		printf("In %.2f degrees is %f radians\n", value, ((value*PI)/180));
		break;
	case'R':
		printf("In %.2f radians is %f degrees\n", value, ((value*180)/PI));
		break;
	default:
		puts("Undefined value!");
	}// switch

	return 0;

}// main