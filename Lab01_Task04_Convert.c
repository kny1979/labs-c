#include <stdio.h>

int main()
{
	char metric;
	int in, ft, cm;
	puts("From which metric system you'll do the conversion? (A - American, E - European)");
	scanf("%c", &metric);
	switch(metric)
	{
	case'E':
		puts("Enter your height in centimeters");
		scanf("%i", &cm);
		ft = (int)(cm / (2.54*12));
		in = ((int)(cm / 2.54)) % 12; // calculation error about 1 inch
		printf("Your height in American metric system is : %i' %i\" \n", ft, in);
		break;
	case'A':
		puts("Enter your height in feet and inches (Format - ft in):");
		scanf("%i %i", &ft, &in);
		printf("Your height in European metric system is is %.1f cm\n", ((ft*12+in)*2.54));
		break;
	}// switch

	return 0;

}// main