#include <stdio.h>
#define SIZE 256
#define WIDTH 44 // Maximum width of the string on the center of the screen (only even value)

int main()
{
	char s[SIZE];
	int i;

	puts("Enter a string: ");
	fgets(s, SIZE, stdin);
	s[strlen(s)-1]=0;
	printf("\n\n");

/* Variant 1 */
	printf("Variant 1:\n");
	for(i=0; i<=((strlen(s)-1)/80); i++)
	{
		if (i<(strlen(s)-1)/80)
			printf("%40.40s%-40.40s", s+80*i, s+40+80*i);
		else 
			printf("%40.*s%-40s\n\n", ((strlen(s)-80*i)/2), s+80*i, s+80*i+((strlen(s)-80*i)/2));
	}

/* Variant 2 - shortest */
	printf("Variant 2:\n");
	for(i=0; i<=((strlen(s)-1)/80); i++)
		printf("%40.*s%-40.40s", ((i<(strlen(s)-1)/80)?40:((strlen(s)-80*i)/2)), s+80*i, s+80*i+((i<(strlen(s)-1)/80)?40:((strlen(s)-80*i)/2)));

/* Variant 3 - Example of solution for even value of WIDTH and WIDTH <= 80 only */
	printf("\n\nVariant 3 (Max width of centered string is %i):\n", WIDTH);
	for(i=0; i<=((strlen(s)-1)/WIDTH); i++)
	{
		if (i<(strlen(s)-1)/WIDTH)
			printf("%40.*s%-40.*s", WIDTH/2, s+WIDTH*i, WIDTH/2, s+WIDTH/2+WIDTH*i);
		else 
			printf("%40.*s%-40s\n", ((strlen(s)-WIDTH*i)/2), s+WIDTH*i, s+WIDTH*i+((strlen(s)-WIDTH*i)/2));
	}

	
	return 0;
}
//* The code listed below is working. But too huge and dummy  *//

////#include <string.h>
	////char ss[39] = "";

	//if ((strlen(s) - 1) > 79)
	//{
	//	for (i = 0; i < (80 * ((strlen(s)-1) /80)); i++)
	//		printf("%c", s[i]);
	//	for (i = 0; i <= (((80 - (strlen(s) - 1 - (80 * ((strlen(s)-1) /80)))) / 2) - 1); i++)
	//		printf(" ");
	//	printf("%s\n", s + ((80 * ((strlen(s)-1) /80))));
	//}

	//if ((strlen(s)-1) == 79)
	//	printf("%s", s);

	//if ((strlen(s)-1) == 77 || (strlen(s)-1) == 78)
	//	printf(" %s\n", s);

	//if ((strlen(s)-1) < 77)
	//{
	//	// Variant #1
	//	memcpy(ss, s, (strlen(s) / 2));
	//	printf("%40s", ss);
	//	printf("%s\n", s + (strlen(s) / 2));

	//	// Variant #2
	//	for (i = 0; i <= ((80 - strlen(s) - 1) / 2); i++)
	//		printf(" ");
	//	printf("%s\n", s);
	//}

