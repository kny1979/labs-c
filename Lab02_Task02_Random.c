#include <stdio.h>
#include <stdlib.h>
#include <time.h>

int main()
{
	srand(time(NULL));
	int count=0, j, i=rand()%101;

	puts("Enter the number in range 0-100: ");

	while(1)
	{
		scanf("%i", &j);
		count++;
		if(j<i)
			puts("More!");
		else if(j>i)
			puts("Less!");
		else
			break;
	}//while

	printf("\nGuessing! \nThe unknown number is: %i! \nThe number of the tries is: %i\n\n", i, count);

	return 0;
}//main