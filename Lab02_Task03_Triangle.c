#include <stdio.h>

int main()
{
	int i, j, lines;  // Variables for Solution 1 and 2
	char s[79]={'0'}; // Variable for Solution 2

	puts("Enter a number of the lines (value must be less or equal 40): ");
	scanf("%i", &lines);

	/* Solution 1 */
	for(i=0; i<(lines); i++)
	{
		for(j=1; j<(lines-i); j++)
			printf(" ");
		for(j=(lines-i); j<=(lines+i); j++)
			printf("*");
		printf("\n");
	}//for
	
	/* Solution 2 */
	for(j=0; j<(lines*2-1); j++)
		s[j]='*';
	for(j=0, i=1; j<lines; j++, i+=2)
		printf("%*.*s\n", lines+j, i, s);

	return 0;

}//main
