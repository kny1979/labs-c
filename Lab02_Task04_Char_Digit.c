#include <stdio.h>
#define STRING "x123abc456def789klm0wyz" // Source string

int main()
{
	char tmp, *endp, arrp[]=STRING, arr[]=STRING;	// Variables for
	int i, j;										// two solutions

	endp=&arrp[strlen(STRING)-1];
	printf("%s   <- Source string.\n", arrp); // Print the source string

/* Solution #1 - Using pointers */
	for(i=strlen(STRING)-1; i>=0; i--) 
	{
		if(arrp[i]>='0' && arrp[i]<='9')
		{
			tmp=arrp[i];
			arrp[i]=*endp;
			*endp=tmp;
			endp-=1;
		}//if
	}//for
	printf("%s   <- Solution #1 Using pointers.\n", arrp); // Print the output string

/* Solution #2 - Without pointers */
	j=strlen(STRING)-1;

	for(i=strlen(STRING)-1; i>=0; i--) 
	{
		if(arr[i]>='0' && arr[i]<='9')
		{
			tmp=arr[j];
			arr[j]=arr[i];
			arr[i]=tmp;
			j--;
		}//if
	}//for
	printf("%s   <- Solution #2 Without pointers\n", arr); // Print the output string

	return 0;
}//main