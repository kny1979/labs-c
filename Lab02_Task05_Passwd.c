#include <stdio.h>
#include <stdlib.h>
#include <time.h>

int main()
{
	int i, j, k;
	srand(time(NULL));

	for(i=1; i<=10; i++)
	{
		for(j=1; j<=4; j++)
		{
			k=rand()%3;
			/* Combination of the two solutions. That's why j<=4. */
			/* Solution - "in one string" */
			printf("%c", ((k<1)?(rand()%('z'-'a')+'a'):((k>1)?(rand()%('Z'-'A')+'A'):(rand()%('9'-'0')+'0')))); 
			/* Simple solution - using switch */
			switch(k)
			{
			case(0):
				printf("%c", rand()%('z'-'a')+'a');
				break;
			case(1):
				printf("%c", rand()%('Z'-'A')+'A');
				break;
			case(2):
				printf("%c", rand()%('9'-'0')+'0');
				break;
			}//switch
		}//for
		printf("\n");
	}//for 
	printf("\n");

	return 0;
}//main