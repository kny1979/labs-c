#include <stdio.h>
#define SIZE 256

char end_space(char *arr, int s);
char beg_space(char *arr, int s);
char mid_space(char *arr, int s1, int s2);

int main()
{
	char arr[SIZE];
	int m=0;
	printf("Enter a string with spaces: \n");
	fgets(arr, SIZE, stdin);
	arr[strlen(arr)-1]=0;

	printf("Source string is: \n");
	printf("\"%s\"\n", arr); 

	if(arr[strlen(arr)-1]==' ') 
		end_space(arr, strlen(arr)-1);

	if(arr[0]==' ') 
		beg_space(arr, 0);

	while(arr[m]!='\0')
	{
		do
		{
			m++;
		}while(arr[m]!=' ');
		mid_space(arr, m, m);
		m++;
	}//while

	printf("Output string is: \n");
	printf("\"%s\"\n", arr); 

	return 0;
}//main

/* Function delete spaces at the end of the string */
char end_space(char *arr, int s)
{
	while(arr[s]==' ')
		s--;
	arr[++s]='\0';
	return arr;
}//func end_space

/* Function delete spaces at the begin of the string */
char beg_space(char *arr, int s)
{
	int i;
	while(arr[s]==' ')
		s++;
	for(i=0; i<strlen(arr); i++)
		arr[i]=arr[i+s];

	return arr;
}// func beg_space

/* Function delete spaces at the middle of the string between the chars and words */
char mid_space(char *arr, int s1, int s2)
{
	int j;
	while(arr[s1+1]==' ')
		s1++;
	if(s1-s2)
	{
		for(j=s2; j<strlen(arr); j++)
			arr[j]=arr[j+(s1-s2)];
	}

	return arr;
}// func mid_space
