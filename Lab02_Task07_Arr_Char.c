#include <stdio.h>
#define SIZE 256

int main()
{
	char arr[SIZE]={0}, symbols[SIZE]={0};
	int a, m=0, d, i, j, count[SIZE]={0};

	puts("Enter a string:");
	fgets(arr, SIZE, stdin);
	arr[strlen(arr)-1]=0;

	printf("\nSource string is: \n\"%s\"\n\n", arr);

	for(i=0; i<strlen(arr); i++)
	{
		a=0;
		for(d=0; d<=m; d++)
		{
			if(arr[i]==symbols[d])
			{
				count[d]+=1;
				a=1; 
			}//if
		}//for

		if(a==0 || symbols[0]=='0')
		{
			count[m]++;
			symbols[m]=arr[i];
			m++; 
		}//if

	}//for

	puts("Count of the symbols in your string:");
	for(j=0; j<strlen(symbols); j++)
		printf("\"%c\" - %d\n", symbols[j], count[j]);

	return 0;
}//main