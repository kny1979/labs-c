#include <stdio.h>
#define SIZE 256

int main()
{
	char s[SIZE]={0};
	int i=0, counter=0;

	puts("Enter a string:");
	fgets(s, SIZE, stdin);
	s[strlen(s)-1]=0;

	printf("Source string is: \n\"%s\"\n", s);

	for(; i<strlen(s); i++)
	{
		if(s[i]!=' ' && s[i]!=',' && s[i]!='(' && s[i]!=')' && s[i]!='.' && s[i]!='!' && s[i]!='?' && s[i]!='"' && s[i]!=':' && s[i]!='\0')
		{
			counter++;
			while(s[i]!=' ' && s[i]!=',' && s[i]!='(' && s[i]!=')' && s[i]!='.' && s[i]!='!' && s[i]!='?' && s[i]!='"' && s[i]!=':' && s[i]!='\0')
				i++;
			i--;
		}//if
	}//for

	printf("Amount of words in the string is: %d\n", counter);

	return 0;
}//main