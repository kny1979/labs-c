#include <stdio.h>
#define SIZE 256

int check_symbol(int *i);
int word_counter(int *i);
const char s[]={'.', ',', ' ', '?', '!', '"', ':', ';', '-', '(', ')', '[', ']', '\0'};
char string[SIZE]={0};
int counter=0;

int main()
{
	int i=0;
	puts("Enter a string");
	fgets(string, SIZE, stdin);
	string[strlen(string)-1]=0;
	printf("\nThe source string is: \"%s\"\n\n", string);
	puts("Words in the source string and its length:");

	for(i=0; i<strlen(string); i++)
	{
		if(check_symbol(&i))
			word_counter(&i);
	}//for

	printf("\nThe amount of words in source string is: %d\n\n", counter);
	
	return 0;
}//main

int check_symbol(int *i)
{
	int j;
	for(j=0; j<strlen(s); j++)
	{
		if(string[*i]==s[j])
			return 0;
	}//for
	return 1;
}//func check_symbol

int word_counter(int *i)
{
	int j, c;
	c=*i;
	counter++;
	printf("\"%c", string[*i]);
	for(*i+=1; *i<strlen(string); *i+=1)
	{
		for(j=0; j<strlen(s); j++)
		{
			if(string[*i]==s[j])
			{
				printf("\" - %d\n", *i-c);
				return *i;
			}//if
		}//for
		printf("%c", string[*i]);
	}
	printf("\" - %d\n", *i-c);
	return *i;
}//func word_counter