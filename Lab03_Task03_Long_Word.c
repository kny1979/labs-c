#include <stdio.h>
#define SIZE 256

int check_symbol(int *i);
int word_counter(int *i);
const char s[]={'.', ',', ' ', '?', '!', '"', ':', ';', '-', '(', ')', '[', ']', '\0'};
char string[SIZE]={0};
int length=0, wb=0;

int main()
{
	int i=0;
	puts("Enter a string");
	fgets(string, SIZE, stdin);
	string[strlen(string)-1]=0;
	printf("\nThe source string is: \"%s\"\n\n", string);
	puts("Most long word in the source string and its length:");

	for(i=0; i<strlen(string); i++)
	{
		if(check_symbol(&i))
			word_counter(&i);
	}//for
	printf("\"");
	for(i=wb; i<(wb+length); i++)
	{
		printf("%c", string[i]);
	}//for
	printf("\" - %d\n\n", length);

	return 0;
}//main

int check_symbol(int *i)
{
	int j;
	for(j=0; j<strlen(s); j++)
	{
		if(string[*i]==s[j])
			return 0;
	}//for
	return 1;
}//func check_symbol

int word_counter(int *i)
{
	int j, c;
	c=*i;
	for(*i+=1; *i<strlen(string); *i+=1)
	{
		for(j=0; j<strlen(s); j++)
		{
			if(string[*i]==s[j])
			{
				if(length<=(*i-c))
				{
					length=*i-c;
					wb=c;
				}//if
				return *i;
			}//if
		}//for
	}
	if(length<=(*i-c))
	{
		length=*i-c;
		wb=c;
	}//if

	return *i;
}//func word_counter