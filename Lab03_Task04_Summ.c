#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#define SIZE 256
#define WIDTH 3

int main()
{
	int i, j, k, p=0, beg, length, sum=0;
	char temp[WIDTH]={0}, string[SIZE]={0};

	printf("Enter a string contains chars and digits (max length of the string is: %d).\n", SIZE);
	printf("If the length of the digital sequence will be longer than %d\n", WIDTH);
	printf("it will be divided into blocks of %d digits each.\n", WIDTH);
	fgets(string, SIZE, stdin);
	string[strlen(string)-1]=0;

	for(i=0; i<strlen(string); i++)
	{
		if(isdigit(string[i]))
		{
			if(p==0)
			{
				printf("Numbers to sum in the source string: \n");
				p++;
			}//if
			beg=i;
			while(isdigit(string[i]))
				i++;
			length=i-beg;
			if(length>=WIDTH)
			{
				for(j=0; j<(length/WIDTH); j++)
				{
					for(k=0; k<WIDTH; k++, beg++)
						temp[k]=string[beg];
					printf("%d ", atoi(temp));
					sum+=atoi(temp);
					for(k=0; k<WIDTH; k++)
						temp[k]='\0';
				}//for
			}//if
			if((i-beg)>0)
			{
				for(k=0; k<=(i-beg); k++, beg++)
					temp[k]=string[beg];
				printf("%d ", atoi(temp));
				sum+=atoi(temp);
				for(k=0; k<WIDTH; k++)
					temp[k]='\0';
			}//if
			i--;
		}//if
	}//for
	if(!sum)
	{
		printf("There are no numbers in the source string\n");
		return 0;
	}//if
	else
		printf("\nSum of the numbers of the source string is: %d\n", sum);

	return 0;
}//main
