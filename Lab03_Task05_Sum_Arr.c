#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#define N 20

int main()
{
	int sum=0, i, k=N-1, arr[N];
	srand(time(NULL));

	printf("Source array of int numbers. Array size is: %d\n", N);
	for(i=0; i<N; i++)
	{
		arr[i]=rand()%N-rand()%N;
		printf("%d ", arr[i]);
	}//for
	printf("\n\n");

	i=0;
	while(arr[i]>0)
		i++;
	printf("The first negative number is: %d\n", arr[i]);

	while(arr[k]<0)
		k--;
	printf("The last positive number is: %d\n\n", arr[k]);

	if(i<k)
	{
		printf("Numbers for the sum is:\n");
		for(i+=1; i<k; i++)
		{
			printf("%d ", arr[i]);
			sum+=arr[i];
		}//for
	}//if
	else
	{
		printf("Operation impossible.\n");
		return 1;
	}//else

	printf("\n\nSum of the numbers between the first negative and the last positive is: %d\n", sum);

	return 0;
}//main