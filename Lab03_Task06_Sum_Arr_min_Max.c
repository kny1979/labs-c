#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#define N 20

int main()
{
	int sum=0, c_min=0, min, c_max=0, max, i, arr[N];
	srand(time(NULL));

	printf("Source array of int numbers. Array size is: %d\n", N);
	for(i=0; i<N; i++)
	{
		arr[i]=rand()%N;
		min=((i==0)?arr[0]:min);
		max=((i==0)?arr[0]:max);
		if(arr[i]<min)
		{
			min=arr[i];
			c_min=i;
		}//if
		if(arr[i]>max)
		{
			max=arr[i];
			c_max=i;
		}//if
		printf("%d ", arr[i]);
	}//for
	printf("\n\n");

	printf("The minimum element of the array is: %d, and its position in the array is: %d\n", min, c_min);
	printf("The maximum element of the array is: %d, and its position in the array is: %d\n", max, c_max);

	if(c_min<c_max)
	{
		printf("\nNumbers for the sum is:\n");
		for(i=c_min+1; i<c_max; i++)
		{
			printf("%d ", arr[i]);
			sum+=arr[i];
		}
	}//if
	else
	{
		printf("\nNumbers for the sum is:\n");
		for(i=c_max+1; i<c_min; i++)
		{
			printf("%d ", arr[i]);
			sum+=arr[i];
		}
	}//else

	printf("\n\nSum of the numbers between the first negative and the last positive is: %d\n", sum);

	return 0;
}//main