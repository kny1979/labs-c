#include <stdio.h>
#define SIZE 256

char symbol, string[SIZE];
int symbols_counter(int *i);
int beg_sequence, end_sequence, counter;

int main()
{
	int i;
	puts("Enter a string:");
	fgets(string, SIZE, stdin);
	string[strlen(string)-1]=0;

	for(i=0; i<strlen(string); i++)
	{
		symbol=((i==0)?string[0]:symbol);
		symbols_counter(&i);
	}//for

	printf("The longest sequence in the string is: \"");
	for(i=beg_sequence; i<end_sequence; i++)
		printf("%c", string[i]);
	printf("\" and its length is: %d\n", counter);

	return 0;
}//main

int symbols_counter(int *i)
{
	int count=0, beg;
	beg=*i;
	
	while(symbol==string[*i])
	{
		count++;
		(*i)++;
	}//while

	if(count>counter)
	{
		beg_sequence=beg;
		counter=count;
		end_sequence=*i;
	}//if
	symbol=string[*i];
	(*i)--;
	return *i;
}//func symbols_counter