#include <stdio.h>
#define SIZE 256

int check_symbol(int *i);
int word_counter(int *i);
const char s[]={'.', ',', ' ', '?', '!', '"', ':', ';', '-', '(', ')', '[', ']', '\0'};
char string[SIZE]={0};
int counter=0, wordNumber;

int main()
{
	int i=0;
	puts("Enter a string");
	fgets(string, SIZE, stdin);
	string[strlen(string)-1]=0;
	puts("Enter a number of the word: ");
	scanf("%i", &wordNumber);
	if(wordNumber<=0)
	{
		puts("Incorrect number of the word!.");
		return 1;
	}

	for(i=0; i<strlen(string); i++)
	{
		if(check_symbol(&i))
			word_counter(&i);
	}//for

	if(wordNumber>counter)
		puts("Incorrect number of the word!");
	
	return 0;
}//main

int check_symbol(int *i)
{
	int j;
	for(j=0; j<strlen(s); j++)
	{
		if(string[*i]==s[j])
			return 0;
	}//for
	return 1;
}//func check_symbol

int word_counter(int *i)
{
	int j, k, c;
	c=*i;
	counter++;

	if(wordNumber==counter)
	{
		printf("\nThe source string is: \"%s\"\n", string);
		printf("Target word is: \n");
		printf("\"%c", string[*i]);
		for(*i+=1; *i<strlen(string); *i+=1)
		{
			for(j=0; j<strlen(s); j++)
			{
				if(string[*i]==s[j])
				{
					printf("\"\n");
					for(k=c; k<strlen(string); k++) // Delete the target word
						string[k]=string[k+(*i-c)]; // from the source string
					printf("Output string is: \n\"%s\"\n", string); // Print output string
					return *i;
				}//if
			}//for
			printf("%c", string[*i]);
		}//for
		printf("\"\n");
		for(k=c; k<strlen(string); k++) // Delete the target word 
			string[k]=string[k+(*i-c)]; // from the source string
		printf("Output string is: \n\"%s\"\n", string); // Print output string
	}//if
	else
	{
		for(*i+=1; *i<strlen(string); *i+=1)
		{
			for(j=0; j<strlen(s); j++)
			{
				if(string[*i]==s[j])
					return *i;
			}//for
		}//for
	}
	return *i;
}//func word_counter