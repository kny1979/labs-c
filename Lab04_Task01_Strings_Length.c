#include <stdio.h>
#define N 10
#define SIZE 256

int main()
{
	int j, i=0;
	char *tmp_ptr, *ptr[N], strings[N][SIZE];

	printf("Enter a few strings of different lengths (length must be less than %d).\n", SIZE);
	printf("Amount of the strings must be less or equal %d.\n", N);
	puts("You may end the strings input with a black line:");
	while(i<N)
	{
		fgets(strings[i], SIZE, stdin);
		strings[i][strlen(strings[i])-1]=0;
		if(!*strings[i])
			break;
		ptr[i]=strings[i];
		for(j=0; j<i; j++)
		{
			if(strlen(ptr[i])>strlen(ptr[j]))
			{
				tmp_ptr=ptr[j];
				ptr[j]=ptr[i];
				ptr[i]=tmp_ptr;
			}//if
		}//for
		i++;
	}//while

	printf("\nYou entered %d string(s).\n", i);
	printf("Strings sorted by length:\n");
	for(j=0;j<i;j++)
		printf("%d. \"%s\"\n", j+1, ptr[j]);

	return 0;
}//main