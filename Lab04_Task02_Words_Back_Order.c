#include <stdio.h>
#define SIZE 256
#define N 100

int check_symbol(int *i, char *s, char *string);
char word_end(int *i, char *s, char *string);

int main()
{
	int i, j, p=0;
	char s[]={'.', ',', ' ', '?', '!', '"', ':', ';', '-', '(', ')', '[', ']', '\0'};
	char *ptr[N], string[SIZE];
	printf("Enter a string (max string length is %d):\n", SIZE);
	fgets(string, SIZE, stdin);
	string[strlen(string)-1]=0;

	for(i=0; i<strlen(string); i++)
	{
		if(check_symbol(&i, s, string))
		{
			ptr[p++]=string+i;
			ptr[p++]=string+word_end(&i, s, string);
		}//if
	}//for

	printf("Output string with back order words:\n\"");
	for(i=0, j=0; i<strlen(string); i++, j+=2)
	{
		while((string+i)!=ptr[j] && i<strlen(string))
			putchar(string[i++]);
		if(j<p)
		{
			while(ptr[j+1]>=ptr[j])
			{
				putchar(*ptr[j+1]--);
				i++;
			}//while
			i--;
		}//if
	}//for
	printf("\"\n");

	return 0;
}//main

int check_symbol(int *i, char *s, char *string)
{
	int j;
	for(j=0; j<strlen(s); j++)
	{
		if(string[*i]==s[j])
			return 0;
	}//for
	return 1;
}//func check_symbol

char word_end(int *i, char *s, char *string)
{
	int j;
	for(*i+=1; *i<strlen(string); *i+=1)
	{
		for(j=0; j<strlen(s); j++)
		{
			if(string[*i]==s[j])
				return --(*i);
		}//for
	}
	return --(*i);
}//func word