#include <stdio.h>
#define SIZE 256

int palindrome(char *ptr);

int main()
{
	char string[SIZE];
	printf("Enter a string (max string length is %d):\n", SIZE);
	fgets(string, SIZE, stdin);
	*(string+(strlen(string)-1))=0;

	printf("The source string is %sa palindrome.\n", (palindrome(string)>0)?"":"not ");

	return 0;
}//main

int palindrome(char *ptr)
{
	char *end=ptr+strlen(ptr)-1;
	while(ptr<=end)
	{
		if(*ptr++!=*end--)
			return 0;
	}//while
	return 1;
}//func palindrome