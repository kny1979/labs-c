#include <stdio.h>
#define SIZE 256

int check_symbol(char *p_str);
char *word_counter(char *p_str);
int counter(char *p_str);

int main()
{
	char string[SIZE]={0};
	puts("Enter a string:");
	fgets(string, SIZE, stdin);
	*(string+strlen(string)-1)=0;
	printf("\nSource string is: \n\"%s\"\n\n", string);
	puts("Words in the string and their length:");
	printf("\nAmount of the words in the string is: %d\n", counter(string));

	return 0;
}//main

int counter(char *p_str)
{
	int counter=0;
	while(*p_str)
	{
		if(check_symbol(p_str))
		{
			counter++;
			p_str=word_counter(p_str);
			continue;
		}//if
		p_str++;
	}//while
	return counter;
}//func counter

int check_symbol(char *p_str)
{
	char separators[]={' ', ',', '.', '?', '!', '"', ':', ';', '-', '(', ')', '[', ']', '\0'};
	char *sprt=separators;
	while(*sprt)
	{
		if(*p_str==*sprt++)
			return 0;
	}
	return 1;
}//func check_symbol

char *word_counter(char *p_str)
{
	char *beg_p_word=p_str;
	printf("\"%c", *p_str++);
	while(*p_str)
	{
		if(!check_symbol(p_str))
		{
			printf("\" - %d\n", p_str-beg_p_word);
			return p_str--;
		}//if
		putchar(*p_str++);
	}//while
	printf("\" - %d\n", p_str-beg_p_word);
	return p_str--;
}//func word_counter