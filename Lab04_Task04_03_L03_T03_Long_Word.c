#include <stdio.h>
#define SIZE 256

int check_symbol(char *p_str);
char *word_counter(char *p_str);
void counter(char *p_str);

int main()
{
	char string[SIZE]={0};
	puts("Enter a string:");
	fgets(string, SIZE, stdin);
	*(string+strlen(string)-1)=0;
	printf("\nSource string is: \n\"%s\"\n", string);
	counter(string);

	return 0;
}//main

void counter(char *p_str)
{
	char *p_beg=p_str, *p_end=p_str, *p_tmp;
	while(*p_str)
	{
		if(check_symbol(p_str))
		{
			p_tmp=p_str;
			p_str=word_counter(p_str);
			if(p_str-p_tmp>p_end-p_beg)
			{
				p_beg=p_tmp;
				p_end=p_str;
			}//if
			continue;
		}//if
		p_str++;
	}//while

	p_tmp=p_beg;
	printf("\nLongest word in the source string and its length is:\n\"");
	while(p_beg<p_end)
		putchar(*p_beg++);
	printf("\" - %d\n\n", p_end-p_tmp);
}//func counter

int check_symbol(char *p_str)
{
	char separators[]={' ', ',', '.', '?', '!', '"', ':', ';', '-', '(', ')', '[', ']', '\0'};
	char *sprt=separators;
	while(*sprt)
	{
		if(*p_str==*sprt++)
			return 0;
	}
	return 1;
}//func check_symbol

char *word_counter(char *p_str)
{
	while(*p_str)
	{
		if(!check_symbol(p_str))
			return p_str--;
		p_str++;
	}//while

	return p_str--;
}//func word_counter