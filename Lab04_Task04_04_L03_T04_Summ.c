#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#define SIZE 256
#define WIDTH 3

int summ(char *p_str);
int pre_summ(char *p_beg, char *pn_beg);

int main()
{
	char string[SIZE]={0};

	printf("Enter a string contains chars and digits (max length of the string is: %d).\n", SIZE);
	printf("If the length of the digital sequence will be longer than %d\n", WIDTH);
	printf("it will be divided into blocks of %d digits each.\n", WIDTH);
	fgets(string, SIZE, stdin);
	string[strlen(string)-1]=0;
	if(!summ(string))
		printf("There are no numbers in the source string\n");

	return 0;
}//main

int summ(char *p_str)
{
	int sum=0;
	char *p_beg;

	while(*p_str)
	{
		if(isdigit(*p_str))
		{
			if(sum==0)
				printf("Numbers to sum in the source string: \n");
			p_beg=p_str;
			while(isdigit(*p_str++));
			p_str--;
			if(p_str-p_beg>=WIDTH)
			{
				while(p_beg<p_beg+((p_str-p_beg)/WIDTH)*WIDTH)
				{
					sum+=pre_summ(p_beg, p_beg+WIDTH);
					p_beg+=WIDTH;
				}//while
			}//if
			if(p_str-p_beg>0)
				sum+=pre_summ(p_beg, p_beg+(p_str-p_beg));
		}//if
		p_str++;
	}//while
	if(sum>0)
		printf("\nSum of the numbers in the source string is: %d\n", sum);
	return sum;
}//func summ

int pre_summ(char *p_beg, char *pn_beg)
{
	char temp[WIDTH]={0};
	char *tmp=temp;
	while(p_beg<pn_beg)
		*tmp++=*p_beg++;
	printf("%d ", atoi(temp));
	return atoi(temp);
}//func pre_summ