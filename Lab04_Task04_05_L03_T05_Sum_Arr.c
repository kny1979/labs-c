#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#define N 20

int *find_neg(int *neg);
int *find_pos(int *pos);
int summ(int *pos, int *neg);

int main()
{
	int arr[N]={0}; 
	int *p_arr=arr;
	printf("Source array of int numbers. Array size is: %d\n", N);
	srand(time(NULL));
	while(p_arr<arr+N)
	{
		*p_arr=rand()%N-rand()%N;
		printf("%d ", *p_arr++);
	}
	printf("\n");
	printf("\nSum of the numbers between the first negative and the last positive is: %d\n", summ(find_pos(arr+N-1), find_neg(arr)));

	return 0;
}//main

int *find_neg(int *neg)
{
	while(*neg++>0);
	neg--;
	printf("The first negative number is: %d\n", *neg);
	return ++neg;
}//func find_neg

int *find_pos(int *pos)
{
	while(*pos--<0);
	pos++;
	printf("The last positive number is: %d\n", *pos);
	return --pos;
}//func find_pos

int summ(int *pos, int *neg)
{
	int sum=0;
	printf("Numbers to sum is:\n");
	while(neg<=pos)
	{
		printf("%d ", *neg);
		sum+=*neg++;
	}//while
	return sum;
}//func summ