#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#define N 20


int *find_min(int *min);
int *find_max(int *max);
int summ(int *min, int *max);

int main()
{
	int arr[N]={0}; 
	int *min, *max, *p_arr=arr;
	printf("Source array of int numbers. Array size is: %d\n", N);
	srand(time(NULL));
	while(p_arr<arr+N)
	{
		*p_arr=rand()%N;
		printf("%d ", *p_arr++);
	}
	printf("\n");
	min=find_min(arr);
	max=find_max(arr);
	printf("\nSum of the numbers between the minimum and the maximum number is: %d\n", ((min<max)?summ(min+1, max-1):summ(max+1, min-1)));
	return 0;
}//main

int *find_min(int *min)
{
	int *p_min=min, *p_end=min+N;
	while(min<p_end)
	{
		p_min=((*p_min>*min)?min:p_min);
		min++;
	}
	printf("The minimum number in the source array is: %d\n", *p_min);
	return p_min;
}//func find_min

int *find_max(int *max)
{
	int *p_max=max, *p_end=max+N;
	while(max<p_end)
	{
		p_max=((*p_max<*max)?max:p_max);
		max++;
	}
	printf("The maximum number in the source array is: %d\n", *p_max);
	return p_max;
}//func find_max

int summ(int *p_min, int *p_max)
{
	int sum=0;
	printf("Numbers to sum is:\n");
	while(p_min<=p_max)
	{
		printf("%d ", *p_min);
		sum+=*p_min++;
	}//while
	return sum;
}//func summ