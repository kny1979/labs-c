#include <stdio.h>
#define SIZE 256

int char_counter(char *p_arr, char *p_symbols, int *p_count);
int check_symbol(char *p_arr, char *p_symb_beg);
int symbol_count(char *p_arr, char *p_arr_beg);
void print_symbols(int max_count, char *p_symbols, int *p_count);

int main()
{
	char arr[SIZE]={0};
	char symbols[SIZE]={0};
	int count[SIZE]={0}, max_count=0;

	puts("Enter a string:");
	fgets(arr, SIZE, stdin);
	*(arr+strlen(arr)-1)=0;

	printf("\nSource string is: \n\"%s\"\n\n", arr);
	max_count=char_counter(arr, symbols, count);
	puts("Chars in the source string and its length:");
	print_symbols(max_count, symbols, count);

	return 0;
}//main

int char_counter(char *p_arr, char *p_symbols, int *p_count)
{
	char *p_arr_beg=p_arr, *p_symb_beg=p_symbols;
	int max_count=0;

	while(*p_arr)
	{
		if(check_symbol(p_arr, p_symb_beg))
			p_arr++;
		else
		{
			*p_symbols++=*p_arr;
			*p_count=symbol_count(p_arr, p_arr_beg);
			max_count=((*p_count>max_count)?*p_count:max_count);
			p_count++;
			p_arr++;
		}//else
	}//while
	return max_count;
}//func char_counter

int check_symbol(char *p_arr, char *p_symb_beg)
{
	while(*p_symb_beg)
	{
		if(*p_arr==*p_symb_beg++)
			return 1;
	}//while
	return 0;
}//func check_symbol

int symbol_count(char *p_arr, char *p_arr_beg)
{
	int count=0;
	while(*p_arr_beg)
	{
		if(*p_arr==*p_arr_beg++)
			count++;
	}//while
	return count;
}//func symbol_count

void print_symbols(int max_count, char *p_symbols, int *p_count)
{
	char *p_symb_beg;
	int *p_count_beg;
	while(max_count>0)
	{
		if(max_count==0)
			break;
		else
		{
			p_symb_beg=p_symbols;
			p_count_beg=p_count;
			while(*p_count_beg)
			{
				if(*p_count_beg==max_count)
					printf("\"%c\" - %d\n", *p_symb_beg, *p_count_beg);
				p_count_beg++;
				p_symb_beg++;
			}//while
			max_count--;
		}//else
	}//while
}//func print_symbols