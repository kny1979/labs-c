#include <stdio.h>
#define SIZE 256

int check_symbol(char *p_str);
char *word_counter(char *p_str);
int counter(char *p_str, int *word_num);
void target_word(char *p_str);

int main()
{
	char string[SIZE]={0};
	int word_num;

	puts("Enter a string:");
	fgets(string, SIZE, stdin);
	*(string+strlen(string)-1)=0;

	puts("Enter a number of the word: ");
	scanf("%i", &word_num);
	if(word_num<=0)
	{
		puts("Incorrect number of the word!.");
		return 1;
	}
	if(counter(string, &word_num))
		puts("Incorrect number of the word!.");

	return 0;
}//main

int counter(char *p_str, int *word_num)
{
	int count=0;
	while(*p_str)
	{
		if(check_symbol(p_str))
		{
			count++;
			if(count==*word_num)
			{
				printf("%d word in the source string is:\n", *word_num);
				target_word(p_str);
				return 0;
			}//if
			else
				p_str=word_counter(p_str);
			continue;
		}//if
		p_str++;
	}//while
	return 1;
}//func counter

int check_symbol(char *p_str)
{
	char separators[]={' ', ',', '.', '?', '!', '"', ':', ';', '-', '(', ')', '[', ']', '\0'};
	char *sprt=separators;
	while(*sprt)
	{
		if(*p_str==*sprt++)
			return 0;
	}
	return 1;
}//func check_symbol

char *word_counter(char *p_str)
{
	while(*p_str)
	{
		if(!check_symbol(p_str))
			return p_str--;
		p_str++;
	}//while
	return p_str--;
}//func word_counter

void target_word(char *p_str)
{
	printf("\"%c", *p_str++);
	while(*p_str)
	{
		if(!check_symbol(p_str))
		{
			printf("\"\n");
			break;
		}//if
		putchar(*p_str++);
	}//while
}//func target_word