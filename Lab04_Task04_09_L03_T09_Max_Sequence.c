#include <stdio.h>
#define SIZE 256

int main()
{
	char string[SIZE];
	char *ptr, *symbol, *beg_sequence=0, *end_sequence=0;

	puts("Enter a string:");
	fgets(string, SIZE, stdin);
	*(string+(strlen(string)-1))=0;
	symbol=string;
	ptr=string;

	while(*ptr)
	{
		while(*symbol==*ptr++);
		*ptr--;
		if((ptr-symbol)>(end_sequence-beg_sequence))
		{
			beg_sequence=symbol;
			end_sequence=ptr;
		}//if
		symbol=ptr;

	}//while

	ptr=beg_sequence;
	printf("The longest sequence in the string is: \"");
	while(beg_sequence<end_sequence)
		putchar(*beg_sequence++);
	printf("\" and its length is: %d\n", end_sequence-ptr);

	return 0;
}//main