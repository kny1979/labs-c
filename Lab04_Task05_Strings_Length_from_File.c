#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#define N 20 //Maximum amount of the strings in txt file
#define SIZE 256 //Maximum string length in the file

int main(int argc, char *argv[])
{
	int j, i=0;
	char *tmp_ptr, *ptr[N], strings[N][SIZE];
	FILE *fptr_in, *fptr_out; 

	if(argc!=3)
	{
		printf("Incorrect syntax in command line!");
		exit(1);
	}//if 
	if((fptr_in=fopen(argv[1], "rt"))==NULL)
	{
		perror("Input file open error.\n");
		exit(2);
	}//if

	puts("Strings readed from the file:");
	while(!feof(fptr_in))
	{
		fgets(strings[i], SIZE, fptr_in);
		strings[i][strlen(strings[i])-1]=0;
		printf("%d. \"%s\"\n", i+1, strings[i]);
		ptr[i]=strings[i];
		for(j=0; j<i; j++)
		{
			if(strlen(ptr[i])>strlen(ptr[j]))
			{
				tmp_ptr=ptr[j];
				ptr[j]=ptr[i];
				ptr[i]=tmp_ptr;
			}//if
		}//for
		i++;
	}//while
	fclose(fptr_in);

	if((fptr_out=fopen(argv[2], "wt"))==NULL)
	{
		perror("Output file open error.\n");
		exit(3);
	}//if

	printf("Strings sorted by length:\n");
	for(j=0;j<i;j++)
	{
		printf("%d. \"%s\"\n", j+1, ptr[j]);
		fputs(ptr[j], fptr_out);
		fputs("\n", fptr_out);
	}//for 
	fclose(fptr_out);
	getchar();

	return 0;
}//main