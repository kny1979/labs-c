#include <stdio.h>
#define N 10
#define NAME 100

int main()
{
	int i=0, age, age_min, age_max;
	char *young, *old, relatives[N][NAME];

	printf("Enter %d or less names of your relatives and they ages.\n", N);
	puts("Blank line instead of the name is the end of the input.");
	while(i<N)
	{
		printf("Enter name of the relative (max %d chars):\n", NAME);
		fgets(relatives[i], NAME, stdin);
		*(relatives[i]+strlen(relatives[i])-1)=0;
		if(!*relatives[i])
			break;
		puts("Enter age of this relative:");
		scanf("%i", &age);
		fflush(stdin);
		age_max=((i==0)?age:age_max);
		age_min=((i==0)?age:age_min);
		if(age_max<=age)
		{
			age_max=age;
			old=relatives[i];
		}//if
		if(age_min>=age)
		{
			age_min=age;
			young=relatives[i];
		}//if
		i++;
	}//while
	printf("Oldest relative is %s, he(she) %d years old.\n", old, age_max);
	printf("Youngest relative is %s, he(she) %d years old.\n", young, age_min);

	return 0;
}//main