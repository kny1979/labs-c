#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#define SIZE 256
#define N 100

int get_words(char *p_str, char **p_words);
int check_symbol(char *p_str);
void print_words(char *p_str, char **p_words, int word_count);

int main()
{
	char string[256]={0};
	char *p_words[N]={0};
	int word_count=0;
	printf("Enter a string (max string length is %d):\n", SIZE);
	fgets(string, SIZE, stdin);
	*(string +strlen(string)-1)=0;
	word_count=get_words(string, p_words);
	print_words(string, p_words, word_count);

	return 0;
}//main

int get_words(char *p_str, char **p_words)
{
	int i=0;
	while(*p_str)
	{
		if(check_symbol(p_str))
		{
			p_words[i++]=p_str;
			while(check_symbol(p_str++));
			p_str--;
		}//if
		else
			p_str++;
	}//while
	return i;
}//func get_words

int check_symbol(char *p_str)
{
	char separators[]={' ', ',', '.', '?', '!', '"', ':', ';', '-', '(', ')', '[', ']', '\0'};
	char *sprt=separators;
	if(!*p_str)
		return 0;
	while(*sprt)
	{
		if(*p_str==*sprt++)
			return 0;
	}
	return 1;
}//func check_symbol

void print_words(char *p_str, char **p_words, int word_count)
{
	int i;
	char *word;
	printf("\nSource string is: \n\"%s\"\n", p_str);
	printf("Output string with random order of words is: \n\"");
	while(*p_str)
	{
		if(check_symbol(p_str))
		{
			i=rand()%(word_count);
			while(p_words[i]==0)
				i=rand()%(word_count);
			word=p_words[i];
			p_words[i]=0;
			while(check_symbol(word))
			{
				putchar(*word++);
			}//while
			while(check_symbol(p_str++));
			p_str--;
		}//if
		else
			putchar(*p_str++);
	}//while
	printf("\"\n");
}//func print_words