#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <windows.h>
#define SIZE 20

void kaleidoskope(void);
void clear_arr(char *arr);
void left_quadrant(char *arr);
void copy_quadrant(char *arr);
void print_kaleidoscope(char arr[SIZE][SIZE]);

int main()
{
	kaleidoskope();
	return 0;
}//main

void kaleidoskope(void)
{
	char arr[SIZE][SIZE];
	while(1)
	{
		clear_arr(arr);
		left_quadrant(arr);
		copy_quadrant(arr);
		system("cls");
		print_kaleidoscope(arr);
		Sleep(1000);
	}//while
}//func kaleidoscope

void clear_arr(char *arr)
{
	int i=0;
	while(i<(SIZE*SIZE))
		*(arr+i++)=' ';
}//func clear_arr

void left_quadrant(char *arr)
{
	int x, y, stars_amount=0;
	srand(time(NULL));
	while(stars_amount<SIZE/2)
		stars_amount=rand()%SIZE+rand()%SIZE;
	while(stars_amount!=0)
	{
		x=rand()%(SIZE/2);
		y=rand()%(SIZE/2);
		if(*(arr+SIZE*x+y)==' ')
		{
			*(arr+SIZE*x+y)='*';
			stars_amount--;
		}//if
	}//while
}//func left_quadrant

void copy_quadrant(char *arr)
{
	int x, y;
	char (*l_ptr)[SIZE], (*r_ptr)[SIZE];
	l_ptr=r_ptr=(char(*)[SIZE])arr;
	for(x=0; x<SIZE/2; x++)
	{
		for(y=0; y<SIZE/2; y++)
			r_ptr[x][SIZE-1-y]=l_ptr[x][y];
	}//for
	for(x=0;x<SIZE/2; x++)
	{
		for(y=0; y<SIZE; y++)
			r_ptr[SIZE-1-x][y]=l_ptr[x][y];
	}//for
}//func copy_quadrant

void print_kaleidoscope(char arr[SIZE][SIZE])
{
	int x, y;
	for(x=0; x<SIZE; x++)
	{
		for(y=0; y<SIZE; y++)
			putchar(arr[x][y]);
		printf("\n");
	}//for
}//func print_kaleidoscope