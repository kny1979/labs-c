#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#define SIZE 256
#define N 100

int get_words(char *p_str, char **p_words);
int check_symbol(char *p_str);
void print_words(FILE* f_str_out, char *p_str, char **p_words);

int main(int argc, char *argv[])
{
	char string[SIZE]={0};
	char *p_words[N]={0};
	FILE *f_str_in, *f_str_out;
	srand(time(NULL));

	if(argc!=3)
	{
		printf("Incorrect syntax in command line!");
		exit(1);
	}//if 
	if((f_str_in=fopen(argv[1], "rt"))==NULL)
	{
		perror("Input file open error.\n");
		exit(2);
	}//if
	if((f_str_out=fopen(argv[2], "wt"))==NULL)
	{
		perror("Output file open error.\n");
		exit(3);
	}//if


	while(fgets(string, SIZE, f_str_in))
	{
		if(*(string+strlen(string)-1)=='\n')
			*(string+strlen(string)-1)=0;
		get_words(string, p_words);
		print_words(f_str_out, string, p_words);
	}
	fcloseall();
	return 0;
}//main

int get_words(char *p_str, char **p_words)
{
	int i=0;
	while(*p_str)
	{
		if(check_symbol(p_str))
		{
			p_words[i++]=p_str;
			while(check_symbol(p_str++));
			p_str--;
			p_words[i++]=p_str-1;
		}//if
		else
			p_str++;
	}//while
	return i;
}//func get_words

int check_symbol(char *p_str)
{
	char separators[]={' ', ',', '.', '?', '!', '"', ':', ';', '-', '(', ')', '[', ']', '\0'};
	char *sprt=separators;
	if(!*p_str)
		return 0;
	while(*sprt)
	{
		if(*p_str==*sprt++)
			return 0;
	}
	return 1;
}//func check_symbol

void print_words(FILE* f_str_out, char *p_str, char **p_words)
{
	int i=0, count=0;
	char *ch;
	while(*p_str)
	{
		if(p_str==p_words[i])
		{
			if((p_words[i+1]-p_words[i])<3)
			{
				while(p_str<=p_words[i+1])
					fputc(*p_str++, f_str_out);
				i+=2;
			}//if
			else
			{
				fputc(*p_str++, f_str_out);
				count=p_words[i+1]-p_words[i]-1;
				while(count!=0)
				{
					ch=p_str+rand()%(p_words[i+1]-p_words[i]-1);
					if(*ch!='0')
					{
						fputc(*ch, f_str_out);
						*ch='0';
						count--;
					}//if
				}//while
				p_str=p_words[i+1];
				fputc(*p_str++, f_str_out);
				i+=2;
			}//else
		}//if
		else
			fputc(*p_str++, f_str_out);
	}//while
	fputc('\n', f_str_out);
}//func print_words