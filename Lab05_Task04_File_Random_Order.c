#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#define SIZE 256
#define N 100

int get_words(char *p_str, char **p_words);
int check_symbol(char *p_str);
void print_words(FILE* f_str_out, char *p_str, char **p_words, int word_count);

int main(int argc, char *argv[])
{
	char string[SIZE]={0};
	char *p_words[N]={0};
	int word_count=0;
	FILE *f_str_in, *f_str_out;

	if(argc!=3)
	{
		printf("Incorrect syntax in command line!");
		exit(1);
	}//if 
	if((f_str_in=fopen(argv[1], "rt"))==NULL)
	{
		perror("Input file open error.\n");
		exit(2);
	}//if
	if((f_str_out=fopen(argv[2], "wt"))==NULL)
	{
		perror("Output file open error.\n");
		exit(3);
	}//if


	while(fgets(string, SIZE, f_str_in))
	{
		if(*(string+strlen(string)-1)=='\n')
			*(string+strlen(string)-1)=0;
		word_count=get_words(string, p_words);
		print_words(f_str_out, string, p_words, word_count);
		word_count=0;
	}
	fcloseall();
	return 0;
}//main

int get_words(char *p_str, char **p_words)
{
	int i=0;
	while(*p_str)
	{
		if(check_symbol(p_str))
		{
			p_words[i++]=p_str;
			while(check_symbol(p_str++));
			p_str--;
		}//if
		else
			p_str++;
	}//while
	return i;
}//func get_words

int check_symbol(char *p_str)
{
	char separators[]={' ', ',', '.', '?', '!', '"', ':', ';', '-', '(', ')', '[', ']', '\0'};
	char *sprt=separators;
	if(!*p_str)
		return 0;
	while(*sprt)
	{
		if(*p_str==*sprt++)
			return 0;
	}
	return 1;
}//func check_symbol

void print_words(FILE* f_str_out, char *p_str, char **p_words, int word_count)
{
	int i;
	char *word;
	while(*p_str)
	{
		if(check_symbol(p_str))
		{
			i=rand()%(word_count);
			while(p_words[i]==0)
				i=rand()%(word_count);
			word=p_words[i];
			p_words[i]=0;
			while(check_symbol(word))
			{
				fputc(*word++, f_str_out);
			}//while
			while(check_symbol(p_str++));
			p_str--;
		}//if
		else
			fputc(*p_str++, f_str_out);
	}//while
	fputc('\n', f_str_out);
}//func print_words