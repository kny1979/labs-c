#include <stdio.h>
#include <math.h>
#define SIZE 40

void clear_arr(char (*arr)[SIZE]);
void fractal(char (*arr)[SIZE], int x_coord, int y_coord, int coeff);
void print_fractal(char (*arr)[SIZE]);

int main()
{
	char arr[SIZE][SIZE];
	int coeff;
	clear_arr(arr);
	puts("Enter a coefficient (0-3): ");
	scanf("%d", &coeff);
	fractal(arr, SIZE/2, SIZE/2, coeff);
	print_fractal(arr);

	return 0;
}//main

void clear_arr(char (*arr)[SIZE])
{
	int x_coord, y_coord;
	for(x_coord=0; x_coord<SIZE; x_coord++)
	{
		for(y_coord=0; y_coord<SIZE; y_coord++)
			arr[x_coord][y_coord]=' ';
	}//for
}//func clear_arr

void fractal(char (*arr)[SIZE], int x_coord, int y_coord, int coeff)
{
	if(coeff==0)
		arr[x_coord][y_coord]='*';
	else
	{
		coeff-=1;
		fractal(arr, x_coord, y_coord, coeff);
		fractal(arr, x_coord+pow(3, coeff), y_coord, coeff);
		fractal(arr, x_coord-pow(3, coeff), y_coord, coeff);
		fractal(arr, x_coord, y_coord+pow(3, coeff), coeff);
		fractal(arr, x_coord, y_coord-pow(3, coeff), coeff);
	}//else
}//func fractal

void print_fractal(char (*arr)[SIZE])
{
	int x_coord, y_coord;
	for(x_coord=0; x_coord<SIZE; x_coord++)
	{
		for(y_coord=0; y_coord<SIZE; y_coord++)
			putchar(arr[x_coord][y_coord]);
		printf("\n");
	}//for
}//func print_fractal