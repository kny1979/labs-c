#include <stdio.h>

void collatz(void);
int recursion(unsigned int i);

int main()
{
	collatz();
	return 0;
}//main

void collatz(void)
{
	int count, number=0, max_length=0;
	unsigned int i;
	for(i=2; i<=1000000; i++)
	{
		count=recursion(i);
		if(count>max_length)
		{
			max_length=count;
			number=i;
		}//if
	}//for
	printf("The number which forms the longest sequence of Collatz\n");
	printf("in range of numbers from 2 to 1000000 is: %d\n", number);
	printf("And it's sequence length is: %d\n\n", max_length);
}//func collatz

int recursion(unsigned int i)
{
	if(i==1)
		return 1;
	else
		return recursion((i%2!=0)?(3*i+1):(i/2))+1;
}//func recursion