#include <stdio.h>
#define SIZE 100

void recursion(int num, char *ch);

int main()
{
	int num;
	char ch[SIZE];
	puts("Enter a number: ");
	scanf("%i", &num);

	recursion(num, ch);
	printf("%s\n", ch);

	return 0;
}//main

void recursion(int num, char *ch)
{
	static int i=0;
	if(num<0)
	{
		ch[i++]='-';
		num=-num;
	}
	if(num/10)
		recursion(num/10,ch);
	ch[i++]='0'+(num%10);
	ch[i]='\0';
}//func recursion