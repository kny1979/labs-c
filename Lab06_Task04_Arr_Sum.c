#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <windows.h>

int degree(int m);
void arr_rand(int *arr, int n);
int arr_summ(int *arr, int n);
int arr_summ_rec(int *arr, int n);

int main()
{
	int *arr, sum=0, sum_rec=0, n=0, m;
	long t;
	srand(time(NULL));
	puts("Enter a value of the degree of two: ");
	scanf("%i", &m);
	n=degree(m);
	arr=(int*)malloc(sizeof(int)*n);
	arr_rand(arr, n);

	t=GetTickCount();
	sum=arr_summ(arr, n);
	t=GetTickCount()-t;
	printf("The sum with the traditional way: %d\n", sum);
	printf("It took %ld milliseconds.\n", t);
	t=GetTickCount();
	sum_rec=arr_summ_rec(arr, n);
	t=GetTickCount()-t;
	printf("The sum with the recursive way: %d\n", sum_rec);
	printf("It took %ld milliseconds.\n", t);
	free(arr);

	return 0;
}//main

int degree(int m)
{
	if(m==0)
		return 1;
	return 2*degree(m-1);
}//func degree

void arr_rand(int *arr, int n)
{
	while(n--)
		*arr++=rand()%100+rand()%(-100);
}//func arr_rand

int arr_summ(int *arr, int n)
{
	int summ=0;
	while(n--)
		summ+=*arr++;
	return summ;
}//func arr_summ

int arr_summ_rec(int *arr, int n)
{
	if(n==1)
		return arr[0];
	return arr_summ_rec(arr, n/2)+arr_summ_rec(arr+n/2, n-n/2);
}//func arr_summ_rec