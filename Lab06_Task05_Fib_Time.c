#include <stdio.h>
#include <windows.h>

int fib_time(int n);

int main()
{
	int fb_t, n;
	long t;
	FILE *fp;
	fp=fopen("fib_time.txt", "wt");
	if(fp==0)
	{
		perror("File error!");
		exit(1);
	}//if
	for(n=1; n<=40; n++)
	{
		t=GetTickCount();
		fb_t=fib_time(n);
		t=GetTickCount()-t;
		printf("Fib %d - %d ", n, fb_t);
		printf(" - it took %ld msec\n", t);
		fprintf(fp, "Fib %d - %d ", n, fb_t);
		fprintf(fp, " - it took %ld msec\n", t);
	}//for
	fcloseall();
	return 0;
}//main

int fib_time(int n)
{
	if(n==1 || n==2)
		return 1;
	return fib_time(n-1)+ fib_time(n-2);
}//func fib_time