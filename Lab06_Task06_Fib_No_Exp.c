#include <stdio.h>

typedef unsigned long long ULL;

ULL base_fib(int fib_num);
ULL short_fib(ULL fst_elem, ULL snd_elem, int fib_num);

int main()
{
	int fib_num;
	for(fib_num=1; fib_num<=70; fib_num++)
		printf("Fib %d - %lld\n", fib_num, base_fib(fib_num));

	return 0;
}//main

ULL base_fib(int fib_num)
{
	return short_fib(0, 1, fib_num);
}//func base_fib

ULL short_fib(ULL fst_elem, ULL snd_elem, int fib_num)
{
	return ((fib_num==1)?snd_elem:short_fib(snd_elem, fst_elem+snd_elem, fib_num-1));
}//func short_fib