#include <stdio.h>
#include <windows.h>

void labirinth(void);
void print_lab(void);
int way_out(int x_coord, int shift_x, int y_coord, int shift_y);

char lab_plan[9][28]={
    "############################",
    "#           #   #          #",
    "##########  #   #          #",
    "#           #   #######  ###",
    "# ######    # x            #",
    "#      #    #   #######   ##",
    "#####  #    #   #         ##",
    "|      #        #     ######",
    "############################"};

int main(void)
{
    labirinth();
    return 0;
}//main

void labirinth(void)
{
	print_lab();
	lab_plan[4][14]=' ';
    if(way_out(4, 0, 14, 0))
        puts("Congratulations!");
}//func labirinth

void print_lab(void)
{
	int x_coord, y_coord;
	system("cls");
    for(x_coord=0; x_coord<=8; x_coord++)
    {
        for(y_coord=0; y_coord<=27; y_coord++)
            putchar(lab_plan[x_coord][y_coord]);
		printf("\n");
    }
    Sleep(200);
}

int way_out(int x_coord, int shift_x, int y_coord, int shift_y)
{
    if(lab_plan[x_coord][y_coord]=='|')
        return 1;
    if(lab_plan[x_coord][y_coord]!=' ')
        return 0;
	lab_plan[x_coord-shift_x][y_coord-shift_y]='.';
	lab_plan[x_coord][y_coord]='x';
    print_lab();
    if(way_out(x_coord, 0, y_coord+1, 1))
        return 1;
    if(way_out(x_coord, 0, y_coord-1, -1))
        return 1;
    if(way_out(x_coord+1, 1, y_coord, 0))
        return 1;
    if(way_out(x_coord-1, -1, y_coord, 0))
        return 1;
	lab_plan[x_coord-shift_x][y_coord-shift_y]='x';
	lab_plan[x_coord][y_coord]='.';
    print_lab();
    return 0;
}//func way_out
