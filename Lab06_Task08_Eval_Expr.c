#include <stdio.h>

struct TNODE
{
	char symb;
	struct TNODE *left;
	struct TNODE *right;
};//struct tree
typedef struct TNODE SNODE;
typedef SNODE *PNODE;

int eval(PNODE root);
PNODE partition(char *expr1, char *expr2);

int main(int argc, char *argv[])
{
	PNODE root;
	root=partition(argv[1], argv[1]+strlen(argv[1])-1);
	printf("Initial expression is: %s\n", argv[1]);
	printf("Expression result is: %d\n", eval(root));
	return 0;
}//main

PNODE partition(char *expr1, char *expr2)
{
	int root_sign=3, sign, bracket=0;
	char *ptr_sign=0, *ptr_tmp=expr1;
	PNODE root =  malloc(sizeof(SNODE));

	if(expr1==expr2)
	{
		root->symb=*expr1;
		root->left=root->right=NULL;
		return root;
	}//if
	while(expr1<=expr2)
	{
		if(*expr1=='(')
		{
			bracket++;
			expr1++;
			continue;
		}//if
		if(*expr1==')')
		{
			bracket--;
			expr1++;
			continue;
		}//if
		if(bracket>0)
		{
			expr1++;
			continue;
		}//if

		switch(*expr1)
		{
		case '+':
		case '-':
			sign=1;
			break;
		case '*':
		case '/':
			sign=2;
			break;
		default:
			sign=3;
			break;
		}//switch
		
		if(sign<=root_sign)
		{
			root_sign=sign;
			ptr_sign=expr1;
		}//if
		expr1++;
	}//while

	if(root_sign==3 && *ptr_tmp=='(' && *expr2==')')
		return partition(ptr_tmp+1, expr2-1);

	root->symb=*ptr_sign;
	root->left=partition(ptr_tmp, ptr_sign-1);
	root->right=partition(ptr_sign+1, expr2);

	return root;
}//func partition

int eval(PNODE root)
{
	int left_number, right_number;
	if(!root->left)
		return root->symb-'0';
	left_number=eval(root->left);
	right_number=eval(root->right);
	switch(root->symb)
	{
	case '+':
		return left_number+right_number;
	case '-':
		return left_number-right_number;
	case '*':
		return left_number*right_number;
	case '/':
		return left_number/right_number;
	}//switch
	return 0;
}//func eval