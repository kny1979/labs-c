#include <stdio.h>

struct WORD
{
	char word[256];
	unsigned int count;
	struct WORD *left;
	struct WORD *right;
};

char *chomp(char *str);
char *buf_clean(char *buf, int word_len);
struct WORD *makeTree(struct WORD *words, char *buf);
void searchTree(struct WORD *words, char *buf);
void printTree(struct WORD *words);

int main(int argc, char *argv[])
{
	FILE *fp_key_words, *fp_source_code;
	char buf[256]={0}, ch;
	int i=0;
	struct WORD *words=0;
	if(argc!=3)
	{
		puts("Incorrect amount of arguments.");
		puts("Syntax: key_words.exe key_words.file source_code.file.");
		return 1;
	}//if
	fp_key_words=fopen(argv[1], "rt");
	fp_source_code=fopen(argv[2], "rt");
	if(!fp_key_words)
	{
		puts("Key words file open error.");
		return 2;
	}//if
	if(!fp_source_code)
	{
		puts("Source code file open error.");
		return 3;
	}//if
	while(!feof(fp_key_words))
	{
		fscanf(fp_key_words, "%s", buf);
		chomp(buf);
		words=makeTree(words, buf);
		buf_clean(buf, strlen(buf));
	}//while
	fclose(fp_key_words);
	while(!feof(fp_source_code))
	{
		ch=fgetc(fp_source_code);
		if(ch>='a' && ch<='z')
		{
			buf[i++]=ch;
		}//if
		else
		{
			if(i)
			{
				searchTree(words, buf);
				i=0;
				buf_clean(buf, strlen(buf));
			}//if
		}//else
	}//while
	fclose(fp_source_code);
	puts("Table of occurrence of keywords in the source code");
	printTree(words);
	return 0;
}//main

char *chomp(char *str)
{
	if(str[strlen(str)-1]=='\n')
		str[strlen(str)-1]=0;
	return str;
}//func chomp

char *buf_clean(char *buf, int word_len)
{
	int i;
	for(i=0; i<word_len; i++)
		buf[i]=0;
	return buf;
}//func buf_clean

struct WORD *makeTree(struct WORD *words, char *buf)
{
	if(words==NULL)
	{
		words=(struct WORD *)malloc(sizeof(struct WORD));
		strcpy(words->word, buf);
		words->count=0;
		words->left=words->right=0;
	}//if
	else if(strcmp(words->word, buf)>0)
		words->left=makeTree(words->left, buf);
	else if(strcmp(words->word, buf)<0)
		words->right=makeTree(words->right, buf);
	return words;
}//func makeTree

void searchTree(struct WORD *words, char *buf)
{
	if(words==NULL)
		return;
	else if(!strcmp(words->word, buf))
		words->count++;
	else if(strcmp(words->word, buf)>0)
		searchTree(words->left, buf);
	else if(strcmp(words->word, buf)<0)
		searchTree(words->right, buf);
	return;
}//func searchTree

void printTree(struct WORD *words)
{
	if(strlen(words->word))
		printf("\"%s\" - %d\n", words->word, words->count);
	if(words->left)
		printTree(words->left);
	if(words->right)
		printTree(words->right);
}//func printTree