#include <stdio.h>

struct SYM
{
	unsigned char ch;
	float freq;
};

int main(int argc, char *argv[])
{
	FILE *fp;
	short flag=0;
	int i, j, ch;
	struct SYM *syms, *tmp_sym;
	unsigned count=0, symb_count=0;
	syms=(struct SYM *)malloc(sizeof(struct SYM)*256);
	tmp_sym=(struct SYM *)malloc(sizeof(struct SYM));
	if(argc!=2)
	{
		puts("Incorret number of arguments.");
		return 1;
	}//if
	fp=fopen(argv[1], "rb");
	if(!fp)
	{
		puts("File error.");
		return 2;
	}//if
	ch=fgetc(fp);
	syms[0].ch=(unsigned char)ch;
	syms[0].freq=0;
	while(ch!=EOF)
	{
		count++;
		for(j=0; j<=symb_count; j++)
		{
			if(syms[j].ch==(unsigned char)ch)
			{
				syms[j].freq++;
				flag=1;
				break;
			}//if
		}//for
		if(!flag)
		{
			syms[++symb_count].ch=(unsigned char)ch;
			syms[symb_count].freq=1;
		}//if
		ch=fgetc(fp);
		flag=0;
	}//for
	fclose(fp);
	for(i=0; i<=symb_count; i++)
		syms[i].freq/=count;
	for(i=0; i<=symb_count; i++)
	{
		for(j=symb_count; j>i; j--)
		{
			if(syms[j-1].freq<syms[j].freq)
			{
				*tmp_sym=syms[j-1];
				syms[j-1]=syms[j];
				syms[j]=*tmp_sym;
			}//if
		}//for
	}//for
	puts("Table of symbol occurrence:");
	for(i=0; i<=symb_count; i++)
		printf("Symbol \"%c\" - frequency of occurrence \"%f\"\n", syms[i].ch, syms[i].freq);
	return 0;
}//main
