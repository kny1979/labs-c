/**********************/
/*    Compressor.c    */ 
/*  Kabargin Nikolay  */ 
/*      05.2015       */ 
/**********************/

#include <stdio.h>
#include <string.h>
#include <malloc.h>

typedef struct SYM
{
	unsigned char ch;
	float freq;
	char code[256];
	struct SYM *left;
	struct SYM *right;
};

union CODE
{
	unsigned char ch;
	struct 
	{
		unsigned short b1:1;
		unsigned short b2:1;
		unsigned short b3:1;
		unsigned short b4:1;
		unsigned short b5:1;
		unsigned short b6:1;
		unsigned short b7:1;
		unsigned short b8:1;
	}byte;
};

char *file_name, *file_ext, *fname_coded, *fname_packed;

unsigned file_size_ext_name(char *file);
int analyser(char *file, struct SYM syms[], struct SYM *psyms[]);
struct SYM *buildTree(struct SYM *psyms[], int count, int new_count);
void makeCodes(struct SYM *root);
unsigned file101_coding(char *file, struct SYM syms[], int count);
void pack_file(int count, struct SYM syms[], unsigned size_of_file, unsigned size_of_packed);
unsigned char pack(char buf[]);

void unpack_file(char *file, struct SYM syms[], struct SYM *psyms[]);
void unpack(unsigned char ch, unsigned char buf[]);
void unpacked_file(struct SYM *root, unsigned char *signature);

int main(int argc, char *argv[])
{
	struct SYM syms[256], *psyms[512], *root;
	int count;
	unsigned size_of_packed=0, size_of_file=0;
	if(argc!=3)
	{
		perror("Incorrect syntax in command line! Syntax: compressor.exe file.xxx [key] \n");
		perror("Key maybe 'p' - for pack file or 'u' - for unpack.\n");
		exit(1);
	}//if 
	size_of_file=file_size_ext_name(argv[1]);
	if(!strcmp("p", argv[2]))
	{
		count=analyser(argv[1], syms, psyms);
		root=buildTree(psyms, count, count);
		makeCodes(root);
		size_of_packed=file101_coding(argv[1], syms, count);
		pack_file(count, syms, size_of_file, size_of_packed);
	}//file for packing
	else if(!strcmp("u", argv[2]))
	{
		unpack_file(argv[1], syms, psyms);
	}//file for unpacking
	else 
	{
		perror("Incorrect syntax in command line! Syntax: compressor.exe file.xxx [key] \n");
		perror("Key maybe 'p' - for pack file or 'u' - for unpack.\n");
		exit(1);
	}
	return 0;
}//main

unsigned file_size_ext_name(char *file)
{
	FILE *initial_file;
	int i=0;
	char ch, *file_beg=file;
	unsigned size=0;
	if((initial_file=fopen(file, "rb"))==NULL)
	{
		perror("Input file open error.\n");
		exit(2);
	}//if
	fseek(initial_file, 0L, SEEK_END);
	size=ftell(initial_file);
	fcloseall();
	while(*file++!='.' && *file);
	if(*file)
	{
		file_name=(char *)calloc(strlen(file_beg)-strlen(file)-1, sizeof(file_name));
		file_ext=(char *)calloc(strlen(file), sizeof(file_ext));
		while(*file_beg!='.')
			*(file_name+i++)=*file_beg++;
		i=0;
		while(*file)
			*(file_ext+i++)=*file++;
	}//if
	else
	{
		file_name=(char *)calloc(strlen(file_beg), sizeof(file_name));
		while(*file_beg)
			*(file_name+i++)=*file_beg++;
		file_ext=(char *)calloc(3, sizeof(file_ext));
		*file_ext='.  ';
	}//else
	return size;
}//func file_ext_and_name

int analyser(char *file, struct SYM syms[], struct SYM *psyms[])
{
	unsigned flag=0, count=0, symb_count=0, i, j;
	int ch;
	struct SYM *tmp_ptr;
	FILE *initial_file;
	if((initial_file=fopen(file, "rb"))==NULL)
	{
		perror("Input file open error.\n");
		exit(2);
	}//if
	ch=fgetc(initial_file);
	syms[0].ch=(unsigned char)ch;
	syms[0].freq=0;
	syms[0].code[0]=0;
	syms[0].left=syms[0].right=0;
	psyms[0]=&syms[0];
	while(ch!=EOF)
	{
		count++;
		for(i=0; i<=symb_count; i++)
		{
			if(syms[i].ch==(unsigned char)ch)
			{
				syms[i].freq++;
				flag=1;
			}//if
		}//for
		if(!flag)
		{
			syms[++symb_count].ch=(unsigned char)ch;
			syms[symb_count].freq=1;
			syms[symb_count].code[0]=0;
			syms[symb_count].left=syms[symb_count].right=0;
			psyms[symb_count]=&syms[symb_count];
		}//if
		ch=fgetc(initial_file);
		flag=0;
	}//while
	fclose(initial_file);
	for(i=0; i<=symb_count; i++)
		syms[i].freq/=count;
	for(i=0; i<=symb_count; i++)
	{
		for(j=symb_count; j>i; j--)
		{
			if(psyms[j-1]->freq<psyms[j]->freq)
			{
				tmp_ptr=psyms[j-1];
				psyms[j-1]=psyms[j];
				psyms[j]=tmp_ptr;
			}//if
		}//for
	}//for
	return symb_count;
}//func analyser

struct SYM *buildTree(struct SYM *psyms[], int count, int new_count)
{
	int i=0;
	struct SYM *tmp_ptr;
	struct SYM *temp=(struct SYM*)malloc(sizeof(struct SYM));
	temp->freq=psyms[count-1]->freq+psyms[count]->freq;
	temp->left=psyms[count];
	temp->right=psyms[count-1];
	temp->code[0]=0;
	if(count==1)
		return temp;
	while(temp->freq<psyms[i]->freq)
		i++;
	new_count++;
	for(;i<=new_count; i++)
	{
		tmp_ptr=psyms[i];
		psyms[i]=temp;
		temp=tmp_ptr;
	}//for
	return buildTree(psyms, count-1, new_count);
}//func buildTree

void makeCodes(struct SYM *root)
{
	if(root->left)
	{
		strcpy(root->left->code,root->code);
		strcat(root->left->code,"0");
		makeCodes(root->left);
	}//if
	if(root->right)
	{
		strcpy(root->right->code,root->code);
		strcat(root->right->code,"1");
		makeCodes(root->right);
	}//if
}//func makeCodes

unsigned file101_coding(char *file, struct SYM syms[], int count)
{
	FILE *initial_file, *coded_file101;
	int ch;
	int i;
	unsigned size=0;
	if((initial_file=fopen(file, "rb"))==NULL)
	{
		perror("Input file open error.\n");
		exit(2);
	}//if
	fname_coded=(char *)calloc(strlen(file_name)+4, sizeof(fname_coded));
	strcpy(fname_coded, file_name);
	strcat(fname_coded, ".101");
	if((coded_file101=fopen(fname_coded, "wb"))==NULL)
	{
		perror("Coded file.101 create error.\n");
		exit(3);
	}//if
	ch=fgetc(initial_file);
	while(ch!=EOF)
	{
		for(i=0; i<=count; i++)
			if(syms[i].ch==(unsigned char)ch)
			{
				fputs(syms[i].code, coded_file101);
				size+=strlen(syms[i].code);
				break;
			}//if
		ch=fgetc(initial_file);
	}//while
	if(size%8)
	{
		for(i=1; i<=8-(size%8); i++)
			fputs("0",coded_file101);
	}//if
	fcloseall();
	return size;
}//func file101_coding

void pack_file(int count, struct SYM syms[], unsigned size_of_file, unsigned size_of_packed)
{
	FILE *coded_file101, *packed_file;
	int i;
	unsigned char buf[8]={0}, ch;
	if((coded_file101=fopen(fname_coded, "rb"))==NULL)
	{
		perror("Coded file.101 file open error.\n");
		exit(6);
	}//if
	fname_packed=(char *)calloc(strlen(file_name)+4, sizeof(fname_packed));
	strcpy(fname_packed, file_name);
	strcat(fname_packed, ".kny");
	if((packed_file=fopen(fname_packed, "wb"))==NULL)
	{
		perror("Packed file.kny create error.\n");
		exit(7);
	}//if

	/*- HEADER -*/
	/*- Signature -*/
	fwrite("kny", sizeof(unsigned char), 3, packed_file);
	/*- Number of unique characters -*/
	fwrite(&count, sizeof(count), 1, packed_file);
	/*- Table of the occurrence -*/
	for(i=0; i<=count; i++)
	{
		fwrite(&syms[i].ch, sizeof(unsigned char), 1, packed_file);
		fwrite(&syms[i].freq, sizeof(float), 1, packed_file);
	}//for
	/*- Tail length -*/
	if(size_of_packed%8)
		i=8-(size_of_packed%8);
	else
		i=0;
	fwrite(&i, sizeof(int), 1, packed_file);
	/*- Initial file length -*/
	fwrite(&size_of_file, sizeof(unsigned), 1, packed_file);
	/*- Initial file extension -*/
	fwrite(file_ext, sizeof(char), 3, packed_file);
	/*- BODY -*/
	for(i=0; i<size_of_packed; i+=8)
	{
		fread(buf, sizeof(unsigned char), 8, coded_file101);
		ch=pack(buf);
		fwrite(&ch, sizeof(unsigned char), 1, packed_file);
	}//while
	fcloseall();
}//func pack_file

unsigned char pack(char buf[])
{
	union CODE code;
	code.byte.b1=buf[0]-'0';
	code.byte.b2=buf[1]-'0';
	code.byte.b3=buf[2]-'0';
	code.byte.b4=buf[3]-'0';
	code.byte.b5=buf[4]-'0';
	code.byte.b6=buf[5]-'0';
	code.byte.b7=buf[6]-'0';
	code.byte.b8=buf[7]-'0';
	return code.ch;
}//func pack

void unpack(unsigned char ch, unsigned char buf[])
{
	union CODE code;
	code.ch=ch;
	buf[0]=code.byte.b1+'0';
	buf[1]=code.byte.b2+'0';
	buf[2]=code.byte.b3+'0';
	buf[3]=code.byte.b4+'0';
	buf[4]=code.byte.b5+'0';
	buf[5]=code.byte.b6+'0';
	buf[6]=code.byte.b7+'0';
	buf[7]=code.byte.b8+'0';

}//func unpack

/*- Decompress -*/
void unpack_file(char *file, struct SYM syms[], struct SYM *psyms[])
{
	int i, j, tail, uni_symb;// i, j - counters
	unsigned size_of_file;
	FILE *packed_file, *coded_file101;
	struct SYM *tmp_ptr, *root;
	unsigned char *signature, ch, buf[8];
	long temp, flen_coded;
	fpos_t file_loc;
	signature=(unsigned char *)calloc(3, sizeof(signature));
	if((packed_file=fopen(file, "rb"))==NULL)
	{
		perror("Packed file.kny open error.\n");
		exit(9);
	}//if
	/*- Check signature -*/
	fread(signature, sizeof(unsigned char), 3, packed_file);
	if(strcmp("kny", signature))
	{
		perror("Signature error. File corrupted or not .kny format.\n");
		exit(10);
	}//if
	/*- Number of unique characters check -*/
	fread(&uni_symb, sizeof(int), 1, packed_file);
	/*- Load table of the occurrence -*/
	for(i=0; i<=uni_symb; i++)
	{
		fread(&syms[i].ch, sizeof(unsigned char), 1, packed_file );
		fread(&syms[i].freq, sizeof(float), 1, packed_file);
		syms[i].code[0]=0;
		syms[i].left=syms[i].right=0;
		psyms[i]=&syms[i];
	}//for
	/*- Tail length -*/
	fread(&tail, sizeof(int), 1, packed_file);
	/*- Load initial file length -*/
	fread(&size_of_file, sizeof(unsigned), 1, packed_file);
	/*- Load initial file extension -*/
	fread(signature, sizeof(char), 3, packed_file);
	/*- Sort by frequency -*/
	for(i=0; i<=uni_symb; i++)
	{
		for(j=uni_symb; j>i; j--)
		{
			if(psyms[j-1]->freq<psyms[j]->freq)
			{
				tmp_ptr=psyms[j-1];
				psyms[j-1]=psyms[j];
				psyms[j]=tmp_ptr;
			}//if
		}//for
	}//for
	/*- Build tree -*/
	root=buildTree(psyms, uni_symb, uni_symb);
	/*- Make codes -*/
	makeCodes(root);
	fname_coded=(char *)calloc(strlen(file_name)+4, sizeof(fname_coded));
	strcpy(fname_coded, file_name);
	strcat(fname_coded, ".101");
	if((coded_file101=fopen(fname_coded, "wb"))==NULL)
	{
		perror("Coded file.101 create error.\n");
		exit(11);
	}//if
	/*- Load body -*/
	temp=ftell(packed_file);
	fgetpos(packed_file, &file_loc);
	fseek(packed_file, 0L, SEEK_END);
	flen_coded=ftell(packed_file)-temp;
	fsetpos(packed_file, &file_loc);
	for(i=0; i<flen_coded-1; i++)
	{
		fread(&ch, sizeof(unsigned char), 1, packed_file);
		unpack(ch, buf);
		for(j=0; j<8; j++)
		{
			fputc(buf[j], coded_file101);
		}
	}//for
	fread(&ch, sizeof(unsigned char), 1, packed_file);
	unpack(ch, buf);
	for(j=0; j<8-tail; j++)
		fputc(buf[j], coded_file101);
	fcloseall();
	unpacked_file(root, signature);
}//func unpack_file

void unpacked_file(struct SYM *root, unsigned char *signature)
{
	FILE *coded_file101, *unpacked_file;
	char ch, *unpacked_file_name;
	int i;
	long flen_coded;
	struct SYM *temp_root=root;
	/*- Open coded 101 file -*/
	if((coded_file101=fopen(fname_coded, "rb"))==NULL)
	{
		perror("Coded file.101 open error.\n");
		exit(12);
	}//if
	/*- Create initial(unpacked file) file -*/
	if(!strcmp(".  ", signature))//if initial file has no extension
	{
		unpacked_file_name=(char *)calloc(strlen(file_name), sizeof(unpacked_file_name));
		strcpy(unpacked_file_name, file_name);
		if((unpacked_file=fopen(unpacked_file_name, "wb"))==NULL)
		{
			perror("Initial(unpacked) file create error.\n");
			exit(13);
		}//if
	}//if
	else
	{
		unpacked_file_name=(char *)calloc(strlen(file_name)+4, sizeof(unpacked_file_name));
		strcpy(unpacked_file_name, file_name);
		strcat(unpacked_file_name, ".");
		strcat(unpacked_file_name, signature);
		if((unpacked_file=fopen(unpacked_file_name, "wb"))==NULL)
		{
			perror("Initial(unpacked) file create error.\n");
			exit(13);
		}//if
	}//else
	fseek(coded_file101, 0L, SEEK_END);
	flen_coded=ftell(coded_file101);
	fseek(coded_file101, 0L, SEEK_SET);
	i=0;
	while(i<flen_coded)
	{
		fread(&ch, sizeof(unsigned char), 1, coded_file101);
		i++;
		while(1)
		{
			if(ch=='0')
				temp_root=temp_root->left;
			if(ch=='1')
				temp_root=temp_root->right;
			if(!temp_root->right && !temp_root->left)
			{
				fwrite(&temp_root->ch, sizeof(unsigned char), 1, unpacked_file);
				temp_root=root;
				break;
			}//if
			else
				fread(&ch, sizeof(unsigned char), 1, coded_file101);
			i++;
		}//while
	}//while
	fcloseall();
}//func unpacked file